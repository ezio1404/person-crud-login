<?php
include '../model/dbconn.php';
if(isset($_POST['addPerson'])){
    $fname= htmlentities($_POST['fname']);
    $lname= htmlentities($_POST['lname']);
    $age= htmlentities($_POST['age']);
    $email= htmlentities($_POST['p_email']);
    $password= htmlentities($_POST['p_password']);
    $data=array($fname,$lname,$age,$email,$password);
    addRecord($data);

    header("location:../index.html?SUccess_register");
}

if(isset($_POST['updatePerson'])){
    $id= $_POST['id'];
    $fname= htmlentities($_POST['fname']);
    $lname= htmlentities($_POST['lname']);
    $age= htmlentities($_POST['age']);
    $email= htmlentities($_POST['p_email']);
    $password= htmlentities($_POST['p_password']);
    $data=array($fname,$lname,$age,$email,$password,$id);
    updateRecord($data);

    header("location:../view/person.php?SUccess_update");
}
if(isset($_POST['deletePerson'])){
deleteRecord(array($_POST['id']));
header("Location:../view/person.php?success_delte");
}












//---------LOG CONTROLLER
if(isset($_POST['signIn'])){
    if(signIn(array($_POST['email'],$_POST['password']))){
        header("location:../view/person.php");
    }else{
      header("location:../index.html?Fail_sign_in");
    }
}
if(isset($_POST['signOut'])){
    session_destroy();
    unset($_SESSION['id']);
    unset($_SESSION['info']);
    header("location:../index.html?succes_logout");
}