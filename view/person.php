<?php
include '../model/dbconn.php';

if($_SESSION){
    $persons=getAllRecord();
    $count=getCount();
 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Person CRUD log</title>
    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand">Person </a>
  <div class="form-inline ">
      <h4><?php echo $_SESSION['info']?>&nbsp;</h4>
      <form action="../controller/personController.php" method="post">
      <input class="btn btn-outline-danger my-2 my-sm-0" type="submit" name="signOut" value="Sign Out">
    </form>
    </div>
</nav>

    <div class="container-fluid">
        <table class="table table-hover table-stripes">
            <thead>
                <th>ID</th>
                <th>NAME</th>
                <th>AGE</th>
                <th>STATUS</th>
                <th >ACTION</th>
            </thead>
            <tbody>
                <?php foreach($persons as $p){ ?>
                <tr>
                    <td><?php echo $p['p_id']; ?></td>
                    <td><?php echo $p['p_lname'].",",$p['p_fname']; ?></td>
                    <td><?php echo $p['p_age']; ?></td>
                    <td><?php echo $p['p_status']; ?></td>
                    <td>
                        <a class="btn btn-primary" href="updateForm.php?id=<?php echo $p['p_id']; ?>">Update</a>
                        <a class="btn btn-warning" href="updateStatus.php?id=<?php echo $p['p_id']; ?>">
                        <?php echo ($p['p_status']=="Active"?"Deactivate":"Activate");?> </a>
                        <a class="btn btn-danger" href="deleteForm.php?id=<?php echo $p['p_id']; ?>">Delete</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</body>
</html>
<?php
}else{
    header("location:../index.html?please_login");
}
?>