<?php
include '../model/dbconn.php';

if($_SESSION){
    $p=getRecord(array($_GET['id']));
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Person CRUD LOG</title>
    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">

</head>
<body>
<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand" href="person.php">Navbar</a>
  <div class="form-inline ">
      <h4><?php echo $_SESSION['info']?>&nbsp;</h4>
      <form action="../controller/personController.php" method="post">
      <input class="btn btn-outline-danger my-2 my-sm-0" type="submit" name="signOut" value="Sign Out">
    </form>
    </div>
</nav>
<div class="container">
<div class="alert alert-danger" role="alert">
<form action="../controller/personController.php" method="post">
                    <p>Do you Want to delete <em  style="text-decoration: underline;"><?php echo $p['p_lname'].",".$p['p_fname'];?> </em>Entry ? </p>
<input type="number" name="id" hidden value="<?php echo $p['p_id'];?>">
                    <input class="btn btn-danger" type="submit" value="Delete" name="deletePerson">
<a class="btn btn-success" href="person.php?cancel_delte" value="Cancel" >Cancel</a>
                </form>
</div>

            </div>
</body>
</html>
<?php
}else{
    header("location:../index.html?please_login");
}
?>