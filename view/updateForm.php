<?php
include '../model/dbconn.php';

if($_SESSION){
    $p=getRecord(array($_GET['id']));
    // print_r($p);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Person CRUD LOG</title>
    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">

</head>
<body>
<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand" href="person.php">Person</a>
  <div class="form-inline ">
      <h4><?php echo $_SESSION['info']?>&nbsp;</h4>
      <form action="../controller/personController.php" method="post">
      <input class="btn btn-outline-danger my-2 my-sm-0" type="submit" name="signOut" value="Sign Out">
    </form>
    </div>
</nav>
<div class="container">
        <form action="../controller/personController.php" method="post">
        <input type="number"class="form-control" name="id" id="id" value="<?php echo $p['p_id']?>" readonly>
                <input class="form-control" type="text" name="fname" id="fname" value="<?php echo $p['p_fname']?>" >
                <input class="form-control" type="text" name="lname" id="lname" value="<?php echo $p['p_lname']?>">
                <input class="form-control" type="number" name="age" id="age" value="<?php echo $p['p_age']?>">
                <input class="form-control" type="email" name="p_email" id="p_email" value="<?php echo $p['p_email']?>">
                <input class="form-control" type="password" name="p_password" id="p_password" value="<?php echo $p['p_password']?>">
                    <input class="btn btn-primary" type="submit" value="Update" name="updatePerson">
                    <a class="btn btn-success" href="person.php?cancel_delte" value="Cancel" >Cancel</a>
                </form>
            </div>
</body>
</html>
<?php
}else{
    header("location:../index.html?please_login");
}
?>