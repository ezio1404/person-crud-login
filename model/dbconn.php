<?php
session_start();
function dbconn(){
    try{
        return new PDO("mysql:hostname=localhost;dbname=person_db","root","");
    }catch(PDOExecption $e){
        echo $e->getMessage();
    } 
}//end dbconn()

function destroy(){
    return null;
}//end of destroy()

function signIn($data){
    $db=dbconn();
    $flag=false;
    $sql="SELECT * FROM tbl_person WHERE p_email=? AND p_password=?";
    $stmt=$db->prepare($sql);
    $stmt->execute($data);
    $user=$stmt->fetch(PDO::FETCH_ASSOC);
    if($stmt->rowCount()>0){
        $_SESSION['id']=$user['p_id'];
        $_SESSION['info']=$user['p_lname'].",".$user['p_fname'];
        $flag=true;
    }
    else{
        echo "<script> alert('Error') </script>";
    }
    $db=destroy();
    return flag;
}

function addRecord($data){
    $db=dbconn();
    $sql="INSERT INTO tbl_person(p_fname,p_lname,p_age,p_email,p_password) values(?,?,?,?,?)";
    $stmt=$db->prepare($sql);
    $stmt->execute($data);
    $db=destroy();
}// end of addRecord

function getAllRecord(){
    $db=dbconn();
    $sql="SELECT * FROM tbl_person";
    $stmt=$db->prepare($sql);
    $stmt->execute();
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $db=destroy();
    return $rows;
}// end of getAllrecord

function getRecord($data){
    $db=dbconn();
    $sql="SELECT * FROM tbl_person WHERE p_id=?";
    $stmt=$db->prepare($sql);
    $stmt->execute($data);
    $row=$stmt->fetch();
    $db=destroy();
    return $row;
}//end of getRecord

function updateRecord($data){
    $db=dbconn();
    $sql="UPDATE tbl_person SET p_fname=?,p_lname=?,p_age=?,p_email=?,p_password=? WHERE p_id=?";
    $stmt=$db->prepare($sql);
    $stmt->execute($data);
    $db=destroy();
}//end of updateRecord

function updateStatus($data){
    $db=dbconn();
    $sql="UPDATE tbl_person SET p_status=? WHERE p_id=?";
    $stmt=$db->prepare($sql);
    $stmt->execute($data);
    $db=destroy();
}//end of updateStatus

function deleteRecord($data){
    $db=dbconn();
    $sql="DELETE FROM tbl_person WHERE p_id=?";
    $stmt=$db->prepare($sql);
    $stmt->execute($data);
    $db=destroy();
}//end of deleteRecord

function getCount(){
    $db=dbconn();
    $sql="SELECT COUNT(*) FROM tbl_person";
    $stmt=$db->prepare($sql);
    $stmt->execute();
    $count=$stmt->fetch();
    $db=destroy();
    return $count;
}// end of getCount
